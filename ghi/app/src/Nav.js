import { NavLink } from 'react-router-dom';
//mockup needs better nav bar formatting , Mike edit the name of your nav links to whatever name you'd like
function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
                <NavLink className="nav-link active" aria-current="page" to="/">
                Home
                </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="manufacturers/">
                Manufacturers List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="manufacturers/new">
                Create a Manufacturer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/models">
                Vehicle Models List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="models/new">
                Create a Vehicle Model
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="automobiles/new">
                Create an Automobile
              </NavLink>
            </li>
            
            <li className="nav-item">
              <NavLink className="nav-link active" to="automobiles/">
                List of Automobiles
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="technicians/new">
                Add a Technician
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="appointments/new">
                Add a Service Appointment
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="appointments/">
                Service Appointments List
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="history/">
                Service History
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="salespersons/new">
                Add a Sales Person
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="customer/new">
                Add a Potential Customer
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="sales/new">
                Add a Sale Record
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="sales/">
                List of All Sales
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="salespersons/sales">
                Salesman Sales History
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, {useEffect, useState} from 'react'
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import NewSalesperson from './SalesPersonForm';
import SalesRecordForm from './SalesForm';
import SalesHistory from './SalesHistory';
import SalesList from './SalesList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturersList from './ManufacturerList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import TechForm from './TechForm';
import AppointmentServiceHistory from './ServiceHistory';
import AutoForm from './AutoForm';  
import AutoList from './AutoList';

function App() {
  // Fetching all our file data here
  const [manufacturerName, setManufacturer] = useState([]);
  const fetchManufacturer = async () => {
      const url = 'http://localhost:8100/api/manufacturers/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setManufacturer(data.manufacturerName)
        }
    }
  
    const [models, setModels] = useState([]);
    const fetchModels = async () => {
      const url = 'http://localhost:8100/api/models/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setModels(data.models)
        }
    }
  
    const [automobile, setAutomobile] = useState([]);
    const fetchAutomobiles = async () => {
      const url = 'http://localhost:8090/api/automobiles/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setAutomobile(data)
        }
    }
  useEffect(() => {
    fetchManufacturer();
    fetchModels();
    fetchAutomobiles();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customer/new" element={< CustomerForm />} />
          <Route path="salespersons/new" element={< NewSalesperson />} />
          <Route path="sales/new" element={< SalesRecordForm />} />
          <Route path="salespersons/sales" element={< SalesHistory /> } />
          <Route path="sales/" element={< SalesList /> } />
          <Route path="models/new" element={ <VehicleModelForm /> } />
          <Route path="models/" element={< VehicleModelList /> } />
          <Route path="manufacturers/" element={ <ManufacturersList /> } />
          <Route path="manufacturers/new" element={< ManufacturerForm /> } />
          <Route path="appointments/new" element={< AppointmentForm /> } />
          <Route path="appointments/" element={< AppointmentList /> } />
          <Route path="history/" element={< AppointmentServiceHistory /> } />
          <Route path="technicians/new" element={< TechForm /> } />
          <Route path="automobiles/new" element={< AutoForm models={models}/>} />
          <Route path="automobiles/" element={< AutoList  /> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
};
export default App;

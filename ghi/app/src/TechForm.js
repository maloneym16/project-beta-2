import { useEffect, useState } from "react";

function TechForm() {
    const [name, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState("");
    const [submitted, setSubmitted] = useState(false);
    const[invalid, setInvalid] = useState(false);
    

	const handleSubmit = async (event) => {
		event.preventDefault();

		const employee_number = employeeNumber;
		const data = { name, employee_number};

		const techUrl = "http://localhost:8080/api/technicians/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};

		const response = await fetch(techUrl, fetchConfig);
		if (response.ok) {
			event.target.reset();
            setName("");
            setEmployeeNumber("");
            setSubmitted(true);
            setInvalid("");
		}else{
            console.error("Error: Invalid Employee Number");
            setInvalid(true);
        }
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1 className="text-center">Create a New Technician</h1>
					<form id="create-appointment-form" onSubmit={handleSubmit}>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setName(e.target.value)}
								placeholder="Name"
								required
								type="text"
								name="name"
								id="name"
								className="form-control"
							/>
							<label htmlFor="name">Tech Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={(e) => setEmployeeNumber(e.target.value)}
								placeholder="employeeNumber"
								required
								type="text"
								name="employeeNumber"
								id="employeeNumber"
								className="form-control"
							/>
							<label htmlFor="employeeNumber">Employee Number</label>
						</div>
						<div className="col text-center">
							<button className="btn btn-primary">Create</button>
						</div>
					</form>
					{invalid && (
						<div
							className="alert alert-danger mb-0 p-4 mt-4"
							id="success-message">
							Sorry , try a different employee number. This is an invalid employee number or that number is already
							taken.
						</div>
					)}
					{!invalid && submitted && (
						<div
							className="alert alert-success mb-0 p-4 mt-4"
							id="success-message">
							Successfully added a new employee!
						</div>
					)}
				</div>
			</div>
		</div>
	);
};
export default TechForm;

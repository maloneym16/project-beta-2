from common.json import ModelEncoder
from .models import Technician, AutoVO, Appointment
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse

# Create your views here.
#do i need more views? come back to this later
class AutoVOEncoder(ModelEncoder):
    model = AutoVO
    properties = [
        "vin_number",
        "vip"
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "vehicle_owner",
        "date_time",
        "technician",
        "reason",
        "finished",
    ]
    encoders = {
    "technician": TechnicianEncoder(),
  }


@require_http_methods(["GET", "POST"])
def api_list_appointments(request,vin=None):
    if request.method == "GET":
        if vin == None:
            appointments = Appointment.objects.all().order_by("date_time")
            return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
        else:
            try:
                appointments = Appointment.objects.filter(vin=vin)
                return JsonResponse(
                    {"appointments":appointments},
                    encoder=AppointmentEncoder
                )
            except Appointment.DoesNotExist:
                response = JsonResponse({"message": "Service Appointment Does Not Exist"})
                response.status_code = 404
                return response
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid tech id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse({"delete":count>0})
    elif request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    else: #PUT
        Appointment.objects.filter(id=pk).update(finished=True)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["GET","POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians":technicians},
            encoder=TechnicianEncoder,
        )
    else:#post
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician":technician},
            encoder=TechnicianEncoder,
            #setting the safe parameter to False actually influences JSON to receive any Python Data Type
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician doesn't exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "Technician deleted"}
            )

        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician doesn't exist"})
            response.status_code = 404
            return response
    else:  # PUT
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

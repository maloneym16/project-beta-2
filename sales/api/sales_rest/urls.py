from django.urls import path

from .views import *

urlpatterns =[
    path("salespersons/", api_sales_person, name="api_sales_person",),

    path("salespersons/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),

    path("customers/", api_customers, name="api_customers"),

    path ("customers/<int:pk>/", show_customer, name="show_customer"),

    path("customers/<int:pk>/", api_delete_customers,name="api_delete_customers",),

    path("sales/", list_sales_records, name="list_sales_records",),

    path("sales/<int:pk>/", show_sales, name="show_sales"),

    path("sales/<int:pk>/", api_delete_sales,name="api_delete_sales",),

    path("automobiles/", api_list_automobiles, name="api_list_automobiles")
]
